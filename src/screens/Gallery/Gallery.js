import React, { Component } from 'react';
import { View, ActivityIndicator, Button, FlatList } from 'react-native';
import ListItem from '../../components/ListItem'
import styles from './styles'

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const { getPhotos } = this.props
        getPhotos()
    }

    openPhoto = uri => {
        const { navigation, openSinglePhoto } = this.props
        openSinglePhoto(uri)

        navigation.navigate('SinglePhoto')
    }

    render() {
        const { navigation, photosData, loading } = this.props;
        console.log('++++++++++++++++++++++++++++++++++'+loading)
        return (
            <View style={styles.container}>
                {loading ? <ActivityIndicator style={{paddingTop: 25}} size='large'/> : null}
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={photosData}
                    renderItem={({ item }) => <ListItem item={item} onPress={() => this.openPhoto(item.urls.regular)} />}
                />
            </View>
        );
    }
}

export default Gallery;
