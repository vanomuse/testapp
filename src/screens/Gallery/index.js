import Gallery from './Gallery';

import { connect } from 'react-redux';

import { photoActions } from '../../redux/Photos';

const mapStateToProps = state => {
  return {
    loading: state.photo.loading,
    photosData: state.photo.data
  };
};

const mapDispatchToProps = dispatch => {
    return {
      getPhotos: () => {
        dispatch(photoActions.getData())
      },
      openSinglePhoto: uri => {
        dispatch(photoActions.setSinglePhoto(uri))
      },
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
