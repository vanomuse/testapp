import React, { Component } from 'react';
import { View, Image } from 'react-native';
import styles from './styles'

class SinglePhoto extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const { image } = this.props;
        return (
            <View style={styles.container}>
                <Image style={styles.image} resizeMode='cover' source={{uri: image}} />
            </View>
        );
    }
}

export default SinglePhoto;
