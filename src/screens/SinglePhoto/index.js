import SinglePhoto from './SinglePhoto';

import { connect } from 'react-redux';


const mapStateToProps = state => {
  return {
    loading: state.photo.loading,
    image: state.photo.singlePhoto
  };
};


export default connect(mapStateToProps)(SinglePhoto);
