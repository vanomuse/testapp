import reducer from './reducer';

import * as photoActions from './actions';
import * as photoTypes from './types';

export { photoActions, photoTypes };

export default reducer;
