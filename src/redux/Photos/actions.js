import * as types from './types';

export const setSinglePhoto = uri => ({
  type: types.SET_SINGLE_PHOTO,
  payload: uri,
});

const setLoading = loading => ({
  type: types.SET_LOADING,
  payload: loading,
});

const setData = data => {
  return {
    type: types.GET_PHOTOS,
    payload: data
  }
}

export const getData = () => async dispatch => {
  dispatch(setLoading(true));
  const reqOptions = { 
    method: 'GET'
  }
  await fetch('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0', reqOptions)
    .then(res => res.json())
    .then(result => dispatch(setData(result)))
    .then(dispatch(setLoading(false)))
    .catch(error => console.log(error))
};
