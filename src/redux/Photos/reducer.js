import * as types from './types'

const initState = {
    data: null,
    loading: false,
    singlePhoto: null
};


const photoReducer = (state = initState, action) => {
    switch (action.type) {
        case types.GET_PHOTOS: {
            return {
                ...state,
                data: action.payload,
            };
        }
        case types.SET_LOADING: {
            return {
                ...state,
                loading: action.payload,
            };
        }
        case types.SET_SINGLE_PHOTO: {
            return {
                ...state,
                singlePhoto: action.payload,
            };
        }
        default:
            return state;
    }
};

export default photoReducer;