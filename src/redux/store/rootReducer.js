import {combineReducers} from 'redux';

import photoReducer from '../Photos/reducer'

const rootReducer = combineReducers({
  photo: photoReducer,
});

export default rootReducer;
