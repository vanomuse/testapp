import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        marginHorizontal: '5%',
        borderWidth: 1,
        borderRadius: 20,
        marginVertical: 20,
        paddingBottom: 10,
    },
    descriptionContainer: {
        marginHorizontal: '5%'
    },
    image: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        height: 200,
    },
    mainText: {
        fontSize: 22,
        fontWeight: '700'
    },
    secondaryText: {
        fontSize: 18
    }

});
