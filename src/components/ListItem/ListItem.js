import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import styles from './styles';

const ListItem = ({ item, onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.container}>
        <Image style={styles.image} source={{ uri: item.urls.small }} />
        <View style={styles.descriptionContainer}>
            <Text style={styles.mainText}>Author: </Text>
            <Text style={styles.secondaryText}>{item.user.name}</Text>
            <Text style={styles.mainText}>Description: </Text>
            <Text style={styles.secondaryText}>{item.alt_description}</Text>
        </View>
    </TouchableOpacity>
);

export default ListItem;
