import React, { Component } from 'react';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import Gallery from './screens/Gallery'
import SinglePhoto from './screens/SinglePhoto'

const AppNavigation = createStackNavigator();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <AppNavigation.Navigator >
            <AppNavigation.Screen
              name="Gallery"
              component={Gallery}
              options={{title: 'Unsplash Gallery Screen'}}
            />
            <AppNavigation.Screen
              name="SinglePhoto"
              component={SinglePhoto}
              options={{title: 'Single Image Screen'}}
            />
          </AppNavigation.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}
export default App;
